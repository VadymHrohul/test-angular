import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FrontSideComponent } from './front-side/front-side.component';
import { BackSideComponent } from './back-side/back-side.component';

@NgModule({
  declarations: [
    AppComponent,
    FrontSideComponent,
    BackSideComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
