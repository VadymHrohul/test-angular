import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FrontSideComponent } from "./front-side/front-side.component";
import { BackSideComponent } from "./back-side/back-side.component";


const routes: Routes = [
  {
    path: '', redirectTo: 'front-side-page', pathMatch: 'full'
  },
  {
    path: 'front-side-page', component: FrontSideComponent
  },
  {
    path: 'back-side-page', component: BackSideComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
